﻿using System;

namespace Hermod.EmailImport.Data {

    using Core.Accounts;
    using Core.Exceptions;

    using Newtonsoft.Json;

    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    /// <summary>
    /// A <see cref="DatabaseConnector"/> which "connects" to an encrypted JSON file containing all domain and user information.
    /// </summary>
    public partial class JsonDatabaseConnector: DatabaseConnector {

        public class DomainContainer {
            public List<Domain> DomainList { get; set; } = new List<Domain>();
        }

        internal FileInfo JsonFile { get; set; }

        private DomainContainer m_jsonObj = new DomainContainer { };

        public JsonDatabaseConnector(FileInfo jsonFile, byte[] key, byte[] initVector) {
            if (jsonFile is null) {
                throw new ArgumentNullException(nameof(jsonFile));
            }
            if (key is null || key.Length == 0) {
                throw new ArgumentNullException(nameof(key));
            }
            if (initVector is null || initVector.Length == 0) {
                throw new ArgumentNullException(nameof(initVector));
            }

            JsonFile = jsonFile;
            m_encKey = key;
            m_initVector = initVector;
        }

        ~JsonDatabaseConnector() {
            DumpJson();
        }

        /// <inheritdoc/>
        public override void Connect() {
            if (!JsonFile.Exists) {
                DumpJson();
                return;
            }
            ReadFile();
        }

        /// <inheritdoc/>
        public override async Task ConnectAsync() {
            if (!JsonFile.Exists) {
                await DumpJsonAsync();
                return;
            }
            await ReadFileAsync();
        }

        internal void ReadFile() {
            var fContents = File.ReadAllBytes(JsonFile.FullName);
            if (fContents is null) {
                DumpJson();
                return;
            }

            string decryptedString = DecryptString(fContents);
            if (decryptedString is null) {
                DumpJson();
                return;
            }

            m_jsonObj = JsonConvert.DeserializeObject<DomainContainer>(decryptedString);
        }

        internal async Task ReadFileAsync() {
            var fContents = await File.ReadAllBytesAsync(JsonFile.FullName);
            if (fContents is null) {
                await DumpJsonAsync();
                return;
            }

            string decryptedString = await DecryptStringAsync(fContents);
            if (decryptedString is null) {
                await DumpJsonAsync();
                return;
            }

            m_jsonObj = JsonConvert.DeserializeObject<DomainContainer>(decryptedString);
        }

        internal void DumpJson() {
            if (!JsonFile.Exists) {
                JsonFile.Directory?.Create();
                JsonFile.Create().Close();
            }

            using var fStream = JsonFile.Open(FileMode.Truncate);
            fStream.Write(EncryptString(JsonConvert.SerializeObject(m_jsonObj, Formatting.Indented)));
        }

        internal async Task DumpJsonAsync() {
            if (!JsonFile.Exists) {
                JsonFile.Directory?.Create();
                JsonFile.Create().Close();
            }

            using var fStream = JsonFile.Open(FileMode.Truncate);
            var encryptedData = await EncryptStringAsync(JsonConvert.SerializeObject(m_jsonObj, Formatting.Indented));
            await fStream.WriteAsync(encryptedData);
        }

        /// <inheritdoc/>
        public override Task<List<Domain>> GetDomainsAsync(bool includeUsers = true, params string[] tlds) {
            if (tlds.Length == 0) {
                return Task.FromResult(
                    includeUsers ?
                        m_jsonObj.DomainList :
                        new List<Domain>(m_jsonObj.DomainList.Select(x => new Domain(x)))
                );
            }

            var filteredDomains = m_jsonObj.DomainList.Where(d => tlds.Contains(d.Tld)).ToList();

            return Task.FromResult(
                includeUsers ?
                    filteredDomains :
                    new List<Domain>(filteredDomains.Select(x => new Domain(x)))
            );
        }

        /// <inheritdoc/>
        public override Task GetUsersForDomainAsync(Domain domain) {
            domain.DomainUsers = m_jsonObj.DomainList.First(x => x.DomainName == domain.DomainName && domain.Tld == x.Tld).DomainUsers.ToList();

            return Task.CompletedTask;
        }

        /// <inheritdoc/>
        public override async Task InitialiseDatabaseAsync() {
            await DumpJsonAsync();
        }

        /// <inheritdoc/>
        public override Task<bool> IsInitialisedAsync() => Task.FromResult(true); // this is always true here

        /// <inheritdoc/>
        /// <remarks >
        /// This will dump the newest version to disk.
        /// </remarks>
        public override async Task<int> PurgeDatabasesAsync() {
            var domainCount = m_jsonObj.DomainList.Count;
            m_jsonObj = new DomainContainer();
            await DumpJsonAsync();

            return domainCount;
        }

        /// <inheritdoc/>
        /// <remarks >
        /// This will dump the newest version to disk.
        /// </remarks>
        public override async Task<int> PurgeUsersFromDomainAsync(Domain domain) {
            var domainToPurge = m_jsonObj.DomainList.First(d => d.Tld == domain.Tld && d.DomainName == domain.DomainName);
            var domainUsers = domainToPurge.DomainUsers.Count;
            domainToPurge.DomainUsers = new List<DomainUser>();
            await DumpJsonAsync();

            return domainUsers;
        }

        /// <inheritdoc/>
        public override async Task<bool> RemoveUserFromDomainAsync(Domain domain, DomainUser user) {
            var domainToEdit = m_jsonObj.DomainList.First(d => d.Tld == domain.Tld && d.DomainName == domain.DomainName);
            domainToEdit.DomainUsers.Remove(user);
            await DumpJsonAsync();

            return true;
        }

        /// <inheritdoc/>
        public override async Task<Domain> AddDomainAsync(string domainName, string serverAddress) {
            string? tld;
            string? domain;

            if (!Domain.IsValidDomain(domainName, out _, out tld, out domain)) {
                if (tld is null || domain is null) {
                    throw new InvalidDomainNameException(domainName, "Encountered malformed domain name!");
                }
            } else if (tld is null || domain is null) {
                throw new Exception("An unknown exception has occurred!"); // this should never happen
            }

            if (m_jsonObj.DomainList.Any(d => d.DomainName == domain && d.Tld == tld)) {
                throw new DomainAlreadyExistsException($"The domain { domainName } already exists in the database!");
            }

            var newDomain = new Domain(m_jsonObj.DomainList.Count + 1, tld, domain);
            newDomain.ServerAddress = serverAddress;
            m_jsonObj.DomainList.Add(newDomain);
            await DumpJsonAsync();

            return newDomain;
        }

        /// <inheritdoc/>
        public override async Task<bool> RemoveDomainAsync(Domain domain) {
            var removed = m_jsonObj.DomainList.Remove(domain);
            await DumpJsonAsync();

            return removed;
        }

        /// <inheritdoc/>
        public override async Task<DomainUser> AddUserToDomainAsync(Domain domain, string user, string password, AccountType accountType) {
            var domainInList = m_jsonObj.DomainList.First(d => domain.Tld == d.Tld && domain.DomainName == d.DomainName);

            byte[] entropy = null;
            DomainUser.GenerateEntropy(ref entropy);

            var saltedPlaintext = password + Encoding.UTF8.GetString(entropy);
            password = null; // Yes, we're not clearing the string but this gives the GC a better chance at cleaning the data

            var encryptedPassword = await EncryptStringAsync(saltedPlaintext);
            saltedPlaintext = null;

            domainInList.DomainUsers.Add(new DomainUser(domainInList.DomainUsers.Count + 1, user, encryptedPassword, entropy, accountType));
            await DumpJsonAsync();

            return domainInList.DomainUsers.Last();
        }

        /// <inheritdoc/>
        public override async Task<string> DecryptUserPassword(DomainUser user) {
            var decryptedPassword = await DecryptStringAsync(user.EncryptedPassword);
            var saltString = Encoding.UTF8.GetString(user.PasswordSalt);
            return decryptedPassword.Substring(0, decryptedPassword.Length - saltString.Length);
        }

        /// <inheritdoc/>
        public override Task SetLastEmailRetrievalAsync(Domain domain, DomainUser user, DateTime? dateTime = null) {
            var listDomain = m_jsonObj.DomainList.First(d => domain == d);

            var domainUser = listDomain.DomainUsers.First(u => u.AccountName == user.AccountName && u.AccountType == user.AccountType);
            domainUser.LastEmailRetrieval = dateTime ?? DateTime.Now;

            return Task.CompletedTask;
        }
    }
}

