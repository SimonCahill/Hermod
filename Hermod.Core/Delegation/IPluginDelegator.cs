﻿using System;

namespace Hermod.Core.Delegation {

    using Commands.Results;

    /// <summary>
    /// Basic contract between Hermod and any laoded plugins which allows jobs to be delegated to other plugins.
    ///
    /// Each plugin will receive an instance of IPluginDelegator (provided by Hermod),
    /// which can then be used to communicate with all other plugins, if need be.
    ///
    /// Each IPluginDelegator will provide a basic means of IPC (inter-plugin communication) and thus
    /// also a means of communicating with Hermod.
    ///
    /// # Publishing a message
    /// When publishing a message for IPC, the topic string must conform to the following pattern:
    /// Failure to adhere to this pattern will result in an exception!
    ///  - Must not be empty or contain only whitespace
    ///  - Must contain only alphanumeric characters or valid wildcards
    ///  - Must begin with a slash (/)
    ///  - Topic levels must be separated by a slah (/)
    ///  - Wildcards must be prefixed by a slash
    ///  - Topics must **not** only consist of only a slash (/)
    ///
    /// Any object type may be published to any topic. Uncaught exceptions will be handled by Hermod.
    /// The publisher will not receive feedback about the published messages.
    /// Published messages will **NOT** be published over any networks!
    /// </summary>
    public interface IPluginDelegator: IMessagePublished {

        /// <summary>
        /// Allows a plugin to subscribe to an individual topic.
        /// </summary>
        /// <param name="topicName">The topic to subscribe to</param>
        /// <exception cref="Exceptions.MalformedTopicException" >If the topic does not meet the topic string requirements.</exception>
        void SubscribeTopic(string topicName);

        /// <summary>
        /// Allows a plugin to subscribe to multiple individual topics.
        /// </summary>
        /// <remarks >
        /// This method will fail silently and ignore any non-conforming topics.
        /// </remarks>
        /// <param name="topics">The topics to subscribe to.</param>
        void SubscribeTopics(params string[] topics);

        /// <summary>
        /// Allows a plugin to unsubscribe from a single topic.
        /// </summary>
        /// <param name="topicName"></param>
        void UnsubscribeTopic(string topicName);

        /// <summary>
        /// Allows a plugin to publish a message on a given topic.
        /// </summary>
        /// <param name="topic">The topic to publish to.</param>
        /// <param name="message">The message to publish to any subscribed plugins.</param>
        void PublishMessage(string topic, object? message);

        /// <summary>
        /// Executes a single command.
        /// </summary>
        /// <remarks >
        /// If command execution has been disabled for this plugin, then a <see cref="Exceptions.CommandExecutionException"/> will be raised.
        /// </remarks>
        /// <param name="command">The command to be executed.</param>
        /// <returns>The result of the command.</returns>
        /// <exception cref="Exceptions.CommandExecutionException" >If an error occurs during command execution.</exception>
        ICommandResult ExecuteCommand(params string[] command);

        /// <summary>
        /// Retrieves an application configuration value.
        /// </summary>
        /// <typeparam name="T">The type of the config.</typeparam>
        /// <param name="config">The config name.</param>
        /// <returns>The desired configuration.</returns>
        /// <exception cref="Exception" >If an error occurs.</exception>
        T GetApplicationConfig<T>(string config);

        /// <summary>
        /// Tries to retrieve an application configuration value.
        /// </summary>
        /// <typeparam name="T">The type of the config.</typeparam>
        /// <param name="config">The config name.</param>
        /// <param name="value">The config value.</param>
        /// <returns><code >true</code> if retrieving the configuration was successful. <code >false</code> otherwise.</returns>
        bool TryGetApplicationConfig<T>(string config, out T? value);

        /// <summary>
        /// Sets a configuration value.
        /// </summary>
        /// <typeparam name="T">The type of the config.</typeparam>
        /// <param name="config">The name of the config to set.</param>
        /// <param name="value">The new value of the config.</param>
        public void SetApplicationConfig<T>(string config, T value);

        /// <summary>
        /// Tries to set an application config.
        /// </summary>
        /// <typeparam name="T">The type of the config.</typeparam>
        /// <param name="config">The name of the config to set.</param>
        /// <param name="value">The new value of the config.</param>
        /// <returns></returns>
        public bool TrySetApplicationConfig<T>(string config, T value);

        /// <summary>
        /// Logs an information message to the logger.
        /// </summary>
        /// <remarks >
        /// This will prefix the message with [name of plugin].
        /// </remarks>
        /// <param name="msg">The message to log.</param>
        /// <param name="args" >The format arguments</param>
        void Information(string msg, params object[] args);

        /// <summary>
        /// Logs a debug message to the logger.
        /// </summary>
        /// <remarks >
        /// This will prefix the message with [name of plugin].
        /// </remarks>
        /// <param name="msg">The message to log.</param>
        /// <param name="args" >The format arguments</param>
        void Debug(string msg, params object[] args);

        /// <summary>
        /// Logs an error message to the logger.
        /// </summary>
        /// <remarks >
        /// This will prefix the message with [name of plugin].
        /// </remarks>
        /// <param name="msg">The message to log.</param>
        /// <param name="args" >The format arguments</param>
        void Error(string msg, params object[] args);

        /// <summary>
        /// Logs a warning message to the logger.
        /// </summary>
        /// <remarks >
        /// This will prefix the message with [name of plugin].
        /// </remarks>
        /// <param name="msg">The message to log.</param>
        /// <param name="args" >The format arguments</param>
        void Warning(string msg, params object[] args);

        /// <summary>
        /// Logs a trace message to the logger.
        /// </summary>
        /// <remarks >
        /// This will prefix the message with [name of plugin].
        /// </remarks>
        /// <param name="msg">The message to log.</param>
        /// <param name="args" >The format arguments</param>
        void Trace(string msg, params object[] args);

    }


}

